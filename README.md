# birdnest-bird

This is a satellite project of birdnet, which is a template for `pytest-bdd` to show case how to setup a BDD project by clone it as template. This project will be a upstream project of `birdnest`, once the pipeline was kickoffed here, it will trigger `birdnest` pipeline.

It will simulate the real user case of continuous integration and continuous delivery/continuous deployment, after deplyment was done. It could start pipeline testing (as integration test) to perform regression test. In case the downstream pipeline failed, it could revert the deployment or don't turn on the traffic.